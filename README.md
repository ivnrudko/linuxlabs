# LinuxLabs

Linux lab works

Базовые утилиты
useradd
passwd
usermod
userdel
groupadd
groupdel
groupmod
groups
id
newgrp
gpasswd
chgrp
chown
chmod

useradd -m -s /bin/bash user1
useradd -m -s /bin/bash user2 

Какие UID создались у пользователей?
[user1@test ~]$ id
uid=1001(user1) gid=1001(user1) groups=1001(user1) context=unconfined_u:unconfined_r:unconfined_t:s0-s0:c0.c1023
[user1@test ~]$
[user2@test ~]$ id
uid=1002(user2) gid=1002(user2) groups=1002(user2) context=unconfined_u:unconfined_r:unconfined_t:s0-s0:c0.c1023
посмотреть можно с помощью команды id или в файле /etc/passwd
user1:x:1001:1001::/home/user1:/bin/bash
user2:x:1002:1002::/home/user2:/bin/bash
Что означают опции -m и -s

m - home directory
s - указываем какой будет shell




